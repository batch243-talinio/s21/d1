// console.log("Happy Monday");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// With array, we can simply write the code above like this:

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];


    // [Section] Array
    /*
        -arrays are used to store multiple related values in a single variable
        -they are declared using square brackets ([]) also known as "Array Literals"
        -commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.
        -array also provide access a number of function/methods that help achieveing specific task.
        -a method is another term for functions associated with an object/array and is used to execute statements that are relevent
        -majority of methods are used to manipulate information stored within the same object
        -arrays are also object which is another type

        Syntax:
        let/const arrayName = [elementA, elementB, elementC, ...];
    */

// common examples of Arrays
let grades = [98.5, 94.4, 89.2, 90.1];
console.log(grades);
let computerBrands = ["Acer", "Asus", "Lenovo", "Dell", "Mac", "Samsung"];
console.log(computerBrands);

// Possible use of an Array but is not recommended.
let mixedArr = [12, 'Asus', null, undefined, {}];
console.log(mixedArr);
// Alternative ways to write array

// console.log(myTasks);

//console.log(myTasks[1].length);

// Create an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];
console.log(cities);

// [Section] length property
// The .length property allows us to get and set the total number of items in an array

console.log(typeof grades.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

let array;
console.log(array);

// length property can also be used with strings 
let fullName = "John Doe";
console.log(fullName.length);

let myTasks = [
    "drink html",
    "eat javascript",
    "inhale css",
    "bake sass"];
console.log(myTasks);

myTasks.length = myTasks.length-1; // we can delete the last element in an array by using decrementation
console.log(myTasks);

// to delete a specific in array we can employ array methods (will be shown in next session s22) or an algorithm.

// this kind of deletion is not applicable in a string.

// in using .length we can also increase an arrays count(lenght) using incrementation(++).

let theBeatles = ["John", "Rudeus", "Kenjiro", "George"];

console.log(theBeatles.length + 1); // not necesarilly needed, it's just for readability and good practice

theBeatles[4] = "Roda";
console.log(theBeatles);


// [Section] Reading/ Accession elements of arrays
    // Accessing array elemets is one of the more common task that we do with an array.
    // This can be done through the use of index.
    // Each element in an aaray is associated w/ it's own index.

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegends[1]);

// we can also save array elemets in another variable.

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// we can also reassign array values using item indices.

console.log("Array before the Reassignment:");
console.log(lakersLegends);

lakersLegends[2] = "Gasol"
console.log("Array after the Reassignment:");
console.log(lakersLegends);


let lastElement = lakersLegends.length-1;

console.log(lakersLegends[lastElement]);

// adding items into the array w/o using array methods.

let newArr = [];
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);


// you can also add at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items on an array.
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// Looping over an array
// you can use a for loop to iterate over all items in an array.

    for (let index = 0; index < newArr.length; index++) {
            console.log(newArr[index]);
    }

let numArr = [5,12,30,46,40];
let divisibleByFive = [];
let notDivisibleByFive = [];
    console.log(numArr[i]);

    for (var i = 0; i < numArr.length; i++) {
        if (numArr[i] % 5 === 0) {
            divisibleByFive[divisibleByFive.length] = numArr[i];
        }else{
            notDivisibleByFive[notDivisibleByFive.length] = numArr[i];
        }
    }

    console.log("This numbers are divisible by 5: " + divisibleByFive);
    console.log("This numbers are not divisible by 5: " + notDivisibleByFive);

// [Section] Multidimensional arrays 
    // are useful for strong complex data structures.
    // a practical application of this is to help vissualize real world objects.
    // useful in number but creating complex array structure is not always recommended.

let chessBoard = [
    ['a1','b1','c1','d1','e1','f1','g1','h1'],
    ['a2','b2','c2','d2','e2','f2','g2','h2'],
    ['a3','b3','c3','d3','e3','f3','g3','h3'],
    ['a4','b4','c4','d4','e4','f4','g4','h4'],
    ['a5','b5','c5','d5','e5','f5','g5','h5'],
    ['a6','b6','c6','d6','e6','f6','g6','h6'],
    ['a7','b7','c7','d7','e7','f7','g7','h7'],
    ['a8','b8','c8','d8','e8','f8','g8','h8']
];

console.table(chessBoard);

